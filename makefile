CC = gcc

SDIR = src
ODIR = obj
EXEC = exe

_OP = ./output

_SRC = main.c 1.c 2.c 3.c
SRC = $(patsubst %,$(SDIR)/%,$(_SRC))

_OBJ = main.o 1.o 2.o 3.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c -o $@ $<

$(EXEC)/output: $(OBJ)
	$(CC) -o $@ $^

.PHONY: clean

run:
	$(EXEC)/$(_OP)

clean:
	rm -f $(ODIR)/*.o $(EXEC)/*
