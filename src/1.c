#include <stdio.h>

typedef struct FREMEONE {
        char VPI:4;
        char GFC:4;
        char VCI:4;
        char VPI1:4;
        char VCI1:8;
        char CLP:1;
        char PTI:3;
        char VCI2:4;
        char HEC:8;
} FRAMEONE;

typedef struct FRAMETWO {
        short int DP;
        short int SP;
        int SN;
        int AN;
        short int window;
        short int codebit:6;
        short int reserved:6;
        short int header:4;
        short int urgent;
        short int checksum;
        int options;
        int data;
} FRAMETWO;

typedef struct FRAMETHREE {
        unsigned char version:4;
        unsigned char HL:4;
        unsigned char ST;
        short int TL;
        short int ID;
        short int flags:3;
        short int FO:13;
        unsigned char TTL;
        unsigned char protocol;
        short int checksum;
        int S_IP;
        int D_IP;
        int option:20;
        int padding:12;
} FRAMETHREE;


void size_of_struct()
{
	printf("Size of Frame One: %ld\n", sizeof(FRAMEONE));
        printf("Size of Frame Two: %ld\n", sizeof(FRAMETWO));
	printf("Size of Frame Three: %ld\n", sizeof(FRAMETHREE));
}
