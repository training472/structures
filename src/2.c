#include <stdio.h>

union sample {
	int a:5;
	int b:10;
	int c:5;
 	int d:21;
 	int e;
};

void print_val(int val)
{
	union sample s;
	s.e = val;

	printf("Values of other members: \na: %d\nb: %d\nc: %d\nd: %d\n", s.a, s.b, s.c, s.d);
}
