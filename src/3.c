#include <stdio.h>

typedef union Tag {
	int data;
	char buff[4];
} Tag;

void find_endian()
{
	Tag t;

	t.data = 1;

	if(t.buff[0])
		printf("little endian \n");

	if(!t.buff[0])
		printf("big endian \n");

}
