#include <stdio.h>
#include <stdlib.h>
#include "../include/header.h"

int main(void)
{
	int ch;

	do {
		printf("-----------------------------------\n");
		printf("Structures\n");
		printf("-----------------------------------\n");

		
			
		printf("\nENTER 0 to EXIT\n\n");

		printf("Enter your choice: ");
		scanf("%d", &ch);

		switch (ch) {
			case 0 : exit(0);
			
			case 1 : printf("\n");
				 size_of_struct();
				 printf("\n\n");

				 break;

			case 2 : printf("\nEnter the value: ");
				 int val;
				 scanf("%d", &val);

				 print_val(val);
				
			 	 printf("\n\n");	 

				 break;

			case 3 : printf("\n");
				 find_endian();
				 printf("\n\n");

				 break;

			default : printf("Invalid choice!!!\n\n");
		}

	} while (1);

	return 0;
}
